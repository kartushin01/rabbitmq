﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DateLib
{
    public class UserGenerator
    {
        public async static Task<Faker<User>> CreateFakerAsync()
        {
          

            var userFaker = new Faker<User>()
               
                .RuleFor(u => u.Name, (f, u) => f.Name.FullName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.Name))
                .RuleFor(u => u.Age, (f, u) => f.Random.Int(14, 100).ToString());

            return userFaker;
        }
    }
}
