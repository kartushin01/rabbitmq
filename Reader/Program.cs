﻿using System;
using System.Text;
using System.Text.Json;
using DateLib;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Reader
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                HostName = "albatross.rmq.cloudamqp.com",
                VirtualHost = "ixdzbevq",
                UserName = "ixdzbevq",
                Password = "4k-oSpb3O3tJDkcwBcW4JWM1rqXA5jae"
            };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "homework",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = JsonSerializer.Deserialize<User>(Encoding.UTF8.GetString(body));
                    if (string.IsNullOrEmpty(message.Email))
                    {
                        return;
                    }
                    Console.WriteLine(" [x] Received {0}", $"Name {message.Name}, Age: {message.Age}, Email: {message.Email}");
                    channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                };
                channel.BasicConsume(
                    queue: "homework",
                    autoAck: false,
                    consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }
    }
}
