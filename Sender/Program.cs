﻿using DateLib;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Text;
using System.Text.Json;
using System.Timers;
namespace Sender
{
    class Program
    {
        private static System.Timers.Timer aTimer;
        public static void Main()
        {

            SetTimer();

            Console.WriteLine("\nPress the Enter key to exit the application...\n");
            Console.WriteLine("The application started at {0:HH:mm:ss.fff}", DateTime.Now);
            Console.ReadLine();
            aTimer.Stop();
            aTimer.Dispose();

            Console.WriteLine("Terminating the application...");

        }

        private static void SetTimer()
        {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(1000);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private static async void OnTimedEvent(Object source, ElapsedEventArgs e)
        {

            User user = await UserGenerator.CreateFakerAsync();

            var factory = new ConnectionFactory() {
                HostName = "albatross.rmq.cloudamqp.com",
                VirtualHost = "ixdzbevq",
                UserName = "ixdzbevq",
                Password = "4k-oSpb3O3tJDkcwBcW4JWM1rqXA5jae"
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "homework",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                string message = JsonConvert.SerializeObject(user);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                    routingKey: "homework",
                    basicProperties: null,
                    body: body);
                Console.WriteLine(" [x] Publish {0}", message);
            }
        }


    }
}
